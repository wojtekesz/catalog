import unittest
from unittest.mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person
from catalog.peselservice import PeselService

class CatalogTest(unittest.TestCase):

    def test_should_get_person_by_pesel(self):
        # given
        p = Person("Miroslaw", "Nowak", "90010100000")

        mock = DB()  # stworzenie obiektu ktory bedzie mockiem, zero magii
        mock.get_person = MagicMock(return_value=p)  # szykujemy zachowanie jednej metody, to jest mock
        c = Catalog(mock, None)  # tworzymy testowany obiekt, spelniajac zaleznosc mockiem

        # when
        result = c.get_person("90010100000")  # wlasciwy test

        # then
        self.assertEqual(p, result)  # weryfikacja po tescie
        mock.get_person.assert_called_with("90010100000")  # dodatkowo weryfikujemy obiekt zastepczy

        # Mozna tez tak, zdefiniowac liste wolan i potem weryfikowac cala liste:
        # calls = [call("pesel")];
        # mock.get_person.assert_has_calls(calls, any_order=False)

    def test_should_get_pesel_by_name_and_surname(self):
        # given
        p = Person('Miroslaw', 'Nowak', ('900101000000','900202000000', '900303000000'))

        mock = DB()
        mock.get_pesel_list = MagicMock(return_value=p.pesel[0])
        c = Catalog(mock, None)

        # when
        result = c.get_pesel_list(p.name, p.surname)

        # then
        self.assertEqual(p.pesel[0], result)
        mock.get_pesel_list.assert_called_with(p.name, p.surname)

    def test_should_get_pesel_list_by_name_and_surname(self):
        # given
        p = Person('Miroslaw', 'Nowak', ('900101000000','900202000000', '900303000000'))

        mock = DB()
        mock.get_pesel_list = MagicMock(return_value=p.pesel)
        c = Catalog(mock, None)

        # when
        result = c.get_pesel_list(p.name, p.surname)

        # then
        self.assertEqual(p.pesel, result)
        mock.get_pesel_list.assert_called_with(p.name, p.surname)

    def test_should_add_person_correct_pesel(self):
        # given
        p = Person('Miroslaw', 'Nowak', '900101000000')

        mock_database = DB()
        mock_service = PeselService()
        mock_database.add_person = MagicMock()
        mock_service.verify = MagicMock(return_value=True)
        c = Catalog(mock_database, mock_service)

        # when
        result = mock_service.verify(p.pesel)
        c.add_person(p.name, p.surname, p.pesel)

        # then
        self.assertTrue(result)
        args = mock_database.add_person.mock_calls[0].args
        self.assertEqual(len(args), 1)
        self.assertEqual(args[0].__class__, Person)
        self.assertEqual(args[0].name, p.name)
        self.assertEqual(args[0].surname, p.surname)
        self.assertEqual(args[0].pesel, p.pesel)

    def test_should_add_person_incorrect_pesel(self):
        # given
        p = Person('Miroslaw', 'Nowak', 'ABCD')

        mock_database = DB()
        mock_service = PeselService()
        mock_database.add_person = MagicMock()
        mock_service.verify = MagicMock(return_value=False)
        c = Catalog(mock_database, mock_service)

        # when
        result = mock_service.verify(p.pesel)
        with self.assertRaises(Exception):
            c.add_person(p.name, p.surname, p.pesel)

        # then
        self.assertFalse(result)