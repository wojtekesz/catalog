import pytest
import mock as mock
from mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person
from catalog.peselservice import PeselService


def person():
    return Person("Miroslaw", "Nowak", ('90010100000', '90020200000', '90030300000'))


@mock.patch('catalog.db.DB.get_person', MagicMock(return_value=person()))
def test_should_get_person_by_pesel():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_person(person().pesel)

    # then
    assert_equal_person(result, person())
    DB.get_person.assert_called_with(person().pesel)


def assert_equal_person(result, expected):
    assert result.get_name() == expected.get_name()
    assert result.get_surname() == expected.get_surname()
    assert result.get_pesel() == expected.get_pesel()


@mock.patch('catalog.db.DB.get_pesel_list', MagicMock(return_value=person().pesel[0]))
def test_should_get_pesel_by_name_and_surname():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_pesel_list(person().name, person().surname)

    # then
    assert result == person().pesel[0]
    DB.get_pesel_list.assert_called_with(person().name, person().surname)


@mock.patch('catalog.db.DB.get_pesel_list', MagicMock(return_value=person().pesel))
def test_should_get_pesel_list_by_name_and_surname():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_pesel_list(person().name, person().surname)

    # then
    assert result == person().pesel
    DB.get_pesel_list.assert_called_with(person().name, person().surname)


@mock.patch('catalog.peselservice.PeselService.verify', MagicMock(return_value=True))
@mock.patch('catalog.db.DB.add_person', MagicMock())
def test_should_add_person_correct_pesel():
    # given
    c = Catalog(DB, PeselService)

    # when
    c.add_person("Miroslaw2", "Nowak2", "91111111111")
    PeselService.verify.assert_called_with("91111111111")

    # then
    args = DB.add_person.call_args.args
    
    assert args[0].__class__ == Person

    expected = Person("Miroslaw2", "Nowak2", "91111111111")
    assert_equal_person(args[0], expected)


@mock.patch('catalog.peselservice.PeselService.verify', MagicMock(return_value=False))
@mock.patch('catalog.db.DB.add_person', MagicMock())
def test_should_add_person_incorrect_pesel():
    # given
    c = Catalog(DB, PeselService)

    # when
    with pytest.raises(Exception):
        c.add_person("Miroslaw2", "Nowak2", "ABCD")

    # then
    PeselService.verify.assert_called_with("ABCD")
    DB.add_person.assert_not_called()